package android.firefly.util;

import android.content.Context;
import android.content.Intent;

public class PowerOnOffUtils {

	private static final String ACTION_FIREFLY_REBOOT  = "action_firefly_reboot";
	
	private static final String ACTION_FIREFLY_SHUTDOWN  = "action_firefly_shutdown";
	private static final String EXTRA_KEY_CONFIRM  = "extra_key_confirm";
	
	private static final String ACTION_FIREFLY_SLEEP  = "action_firefly_sleep";
	
	public static void shutdown(Context context ,boolean showConfirm){
	    Intent intent = new Intent(ACTION_FIREFLY_SHUTDOWN);  
	    intent.putExtra(EXTRA_KEY_CONFIRM, showConfirm); 
	    context.sendBroadcast(intent);
	}
	
	public static void reboot(Context context){
	    Intent intent = new Intent(ACTION_FIREFLY_REBOOT);  
	    context.sendBroadcast(intent);
	}
	
	public static void sleep(Context context){
	    Intent intent = new Intent(ACTION_FIREFLY_SLEEP);  
	    context.sendBroadcast(intent);
	}
}